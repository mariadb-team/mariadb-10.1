---
include:
  - https://salsa.debian.org/lts-team/pipeline/raw/master/recipes/stretch.yml

# Overridfe Salsa-CI with MariaDB specific variations
variables:
  SALSA_CI_DISABLE_REPROTEST: 1
  DEB_BUILD_OPTIONS: 'noautodbgsym'

stages:
  - provisioning
  - build
  - test quality
  - upgrade in Stretch
  - upgrade from Jessie
  - test extras
  - test # Stage referenced by Salsa-CI template reprotest stanza, so must exist
  - publish # Stage referenced by Salsa-CI template aptly stanza, so must exist even though not used

# In addition to Salsa-CI, also run these fully MariaDB specific build jobs

fresh install:
  stage: test quality
  needs:
    - job: build
      artifacts: true
  image: "$SALSA_CI_IMAGES_BASE"
  artifacts:
    when: always
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - sed -i "s/101/0/g" -i /usr/sbin/policy-rc.d # Enable automatic restarts from maint scripts
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - apt-get update
    # Install MariaDB built in this commit
    - apt-get install -y ./*.deb
    # Verify installation of MariaDB built in this commit
    - dpkg -l | grep -iE 'maria|mysql|galera' || true # List installed
    - mariadb --version # Client version
    - service mysql status
    - mkdir -p debug # Ensure dir exists before using it
    - find /var/lib/mysql -ls > debug/var-lib-mysql.list
    - cp -ra /etc/mysql debug/etc-mysql
    - cp -ra /var/log/mysql debug/var-log-mysql
    - mariadb --skip-column-names -e "select @@version, @@version_comment" # Show version
    - echo 'SHOW DATABASES;' | mariadb # List databases
    - mariadb -e "create database test; use test; create table t(a int primary key) engine=innodb; insert into t values (1); select * from t; drop table t; drop database test;" # Test InnoDB works
  variables:
    GIT_STRATEGY: none
  except:
    variables:
      - $CI_COMMIT_TAG != null && $SALSA_CI_ENABLE_PIPELINE_ON_TAGS !~ /^(1|yes|true)$/

mariadb-10.1.x to mariadb-10.1.y upgrade:
  stage: upgrade in Stretch
  needs:
    - job: build
      artifacts: true
  image: "$SALSA_CI_IMAGES_BASE"
  artifacts:
    when: always
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - sed -i "s/101/0/g" -i /usr/sbin/policy-rc.d # Enable automatic restarts from maint scripts
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - apt-get update
    # Install almost everything currently in Debian Stretch
    - apt-get install -y 'default-mysql*' 'mariadb-*' 'libmariadbd*' 'libmariadbclient*'
    # Verify installation of MariaDB currently in Debian Stretch
    - dpkg -l | grep -iE 'maria|mysql|galera' || true # List installed
    - service mysql status
    - mariadb --skip-column-names -e "select @@version, @@version_comment"
    - echo 'SHOW DATABASES;' | mysql
    # Install MariaDB built in this commit
    - apt-get install -y ./*.deb || true # Allow to proceed so debug artifacts get collected
    # Verify installation of MariaDB built in this commit
    - dpkg -l | grep -iE 'maria|mysql|galera' || true # List installed
    - mariadb --version # Client version
    - service mysql status
    - mkdir -p debug # Ensure dir exists before using it
    - find /var/lib/mysql -ls > debug/var-lib-mysql.list
    - cp -ra /etc/mysql debug/etc-mysql
    - cp -ra /var/log/mysql debug/var-log-mysql
    - mariadb --skip-column-names -e "select @@version, @@version_comment" # Show version
    - echo 'SHOW DATABASES;' | mariadb # List databases before upgrade are still there
    - mariadb -e "create database test; use test; create table t(a int primary key) engine=innodb; insert into t values (1); select * from t; drop table t; drop database test;" # Test InnoDB works
  variables:
    GIT_STRATEGY: none
  except:
    variables:
      - $CI_COMMIT_TAG != null && $SALSA_CI_ENABLE_PIPELINE_ON_TAGS !~ /^(1|yes|true)$/


test basic features:
  stage: test quality
  needs:
    - job: build
      artifacts: true
  image: "$SALSA_CI_IMAGES_BASE"
  artifacts:
    when: always
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - sed -i "s/101/0/g" -i /usr/sbin/policy-rc.d # Enable automatic restarts from maint scripts
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - apt-get update
    # Install MariaDB built in this commit
    - apt-get install -y ./*.deb
    # Verify installation of MariaDB built in this commit
    - dpkg -l | grep -iE 'maria|mysql|galera' || true # List installed
    - mariadb --version # Client version
    - service mysql status
    - mkdir -p debug # Ensure dir exists before using it
    - find /var/lib/mysql -ls > debug/var-lib-mysql.list
    - cp -ra /etc/mysql debug/etc-mysql
    - cp -ra /var/log/mysql debug/var-log-mysql
    - echo 'SHOW DATABASES;' | mariadb # List databases
    # Print info about server
    - mariadb --skip-column-names -e "select @@version, @@version_comment"
    - mariadb --skip-column-names -e "select engine, support, transactions, savepoints from information_schema.engines order by engine" | sort
    - mariadb --skip-column-names -e "select plugin_name, plugin_status, plugin_type, plugin_library, plugin_license from information_schema.all_plugins order by plugin_name, plugin_library"
    # Test various features
    - mariadb -e "CREATE DATABASE db"
    - mariadb -e "CREATE TABLE db.t_innodb(a1 SERIAL, c1 CHAR(8)) ENGINE=InnoDB; INSERT INTO db.t_innodb VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE TABLE db.t_myisam(a2 SERIAL, c2 CHAR(8)) ENGINE=MyISAM; INSERT INTO db.t_myisam VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE TABLE db.t_aria(a3 SERIAL, c3 CHAR(8)) ENGINE=Aria; INSERT INTO db.t_aria VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE TABLE db.t_memory(a4 SERIAL, c4 CHAR(8)) ENGINE=MEMORY; INSERT INTO db.t_memory VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE ALGORITHM=MERGE VIEW db.v_merge AS SELECT * FROM db.t_innodb, db.t_myisam, db.t_aria"
    - mariadb -e "CREATE ALGORITHM=TEMPTABLE VIEW db.v_temptable AS SELECT * FROM db.t_innodb, db.t_myisam, db.t_aria"
    - mariadb -e "CREATE PROCEDURE db.p() SELECT * FROM db.v_merge"
    - mariadb -e "CREATE FUNCTION db.f() RETURNS INT DETERMINISTIC RETURN 1"
    # Test that the features still work (this step can be done e.g. after an upgrade)
    - mariadb -e "SHOW TABLES IN db"
    - mariadb -e "SELECT * FROM db.t_innodb; INSERT INTO db.t_innodb VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT * FROM db.t_myisam; INSERT INTO db.t_myisam VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT * FROM db.t_aria; INSERT INTO db.t_aria VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT * FROM db.t_memory; INSERT INTO db.t_memory VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT COUNT(*) FROM db.v_merge"
    - mariadb -e "SELECT COUNT(*) FROM db.v_temptable"
    - mariadb -e "CALL db.p()"
    - mariadb -e "SELECT db.f()"
  variables:
    GIT_STRATEGY: none
  except:
    variables:
      - $CI_COMMIT_TAG != null && $SALSA_CI_ENABLE_PIPELINE_ON_TAGS !~ /^(1|yes|true)$/

# Build a piece of software that was designed for libmysqlclient-dev but using the
# libmariadb-dev-compat layer. Should always end up using libmariadb.so.3 run-time.
build mariadbclient consumer Python-MySQLdb:
  stage: test quality
  needs:
    - job: build
      artifacts: true
  image: "$SALSA_CI_IMAGES_BASE"
  script:
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - mkdir -p debug # Ensure dir exists before using it
    - apt-get update
    - apt-get install -y pkg-config ./libmariadbclient-dev*.deb ./libmariadbclient18_*.deb ./mariadb-common*.deb
    - pkg-config --list-all # See what MySQLdb builds with
    - ldconfig -p | grep -e mariadb -e mysql
    - mysql_config # Must be present for mysqlclient Python module to build
    - apt-get install -y python3-pip
    - pip3 install mysqlclient # Compiles module against libmysqlclient
    - apt-get purge -y libmariadb-dev # Not needed for run-time
    - python3 -c "import MySQLdb; print(MySQLdb.get_client_info())"
  variables:
    GIT_STRATEGY: none
  except:
    variables:
      - $CI_COMMIT_TAG != null && $SALSA_CI_ENABLE_PIPELINE_ON_TAGS !~ /^(1|yes|true)$/

